//
//  FirebaseManager.swift
//  HelpMeOut
//
//  Created by allzone on 9/24/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import Foundation
import Firebase

class FIRManager {
    static let shared = FIRManager()
    
    let authManager = FIRAuth.auth()
    
    // MARK: Database methods
    
    func saveRecordTo(table: String, data: NSDictionary) {
        let ref = FIRDatabase.database().referenceWithPath(table)
        ref.updateChildValues(data as [NSObject : AnyObject])
    }
    
    func deleteRecordTo(table: String) {
        let ref = FIRDatabase.database().referenceWithPath(table)
        ref.removeValue()
    }
    
    func loadFromTableOnce(table: String, completion:(results: NSDictionary) -> Void) {
        let ref = FIRDatabase.database().referenceWithPath(table)
        ref.observeSingleEventOfType(.Value, withBlock: { snapshot in
            if let result = snapshot.value as? NSDictionary {
                completion(results: result)
            } else {
                completion(results: [:])
            }
        })
    }
    
    func loadFromTable(table: String, completion:(results: NSDictionary) -> Void) {
        let ref = FIRDatabase.database().referenceWithPath(table)
        ref.observeEventType(.Value, withBlock: { snapshot in
            if let result = snapshot.value as? NSDictionary {
                completion(results: result)
            } else {
                completion(results: [:])
            }
        })
    }
    
    // MARK: Auth methods
    func createUser(email: String, password: String, completion:(user: FIRUser?, error: NSError?) -> Void) {
        authManager?.createUserWithEmail(email, password: password, completion: { (user, error) in
            completion(user: user, error: error)
        })
    }
    
    func loginUser(email: String, password: String, completion:(user: FIRUser?, error: NSError?) -> Void) {
        authManager?.signInWithEmail(email, password: password, completion: { (user, error) in
            completion(user: user, error: error)
        })
    }
    
    func checkUserIsLoggedin(handler:() -> Void) {
        FIRAuth.auth()?.addAuthStateDidChangeListener { auth, user in
            if let _ = user {
                // User is signed in
                handler()
            }
        }
    }
    
    // MARK: Notifications
    
    func notifyCaregivers(currentUserID: String, notification: (title: String, body: String), deviceToken: String, handler:(response: NSURLResponse?, error: NSError?) -> Void) {
        let request = NSMutableURLRequest(URL: APNConfig.BaseURL)
        request.HTTPMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue("key=\(APNConfig.APIKey)", forHTTPHeaderField: "Authorization")
        request.setValue("id=\(APNConfig.SenderID)", forHTTPHeaderField: "Sender")

        let json = ["notification":["title":notification.title, "body":notification.body],"priority" :"high", "to":deviceToken]
        
        let jsonData = try! NSJSONSerialization.dataWithJSONObject(json, options: .PrettyPrinted)
        
        request.HTTPBody = jsonData
        
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
            handler(response: response, error: error)
            guard error == nil && data != nil else {
                print("error=\(error)")
                return
            }
            
            if let httpStatus = response as? NSHTTPURLResponse where httpStatus.statusCode != 200 {           // check for http errors
                print("statusCode should be 200, but is \(httpStatus.statusCode)")
                print("response = \(response)")
            }
            
            let responseString = String(data: data!, encoding: NSUTF8StringEncoding)
            print("responseString = \(responseString)")
        }
        task.resume()
    }
}
