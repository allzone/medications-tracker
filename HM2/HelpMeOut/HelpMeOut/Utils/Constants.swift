//
//  Constants.swift
//  HelpMeOut
//
//  Created by Ion Brumari on 9/23/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import Foundation

struct Segues {
    static let ShowProfile = "ShowProfileSegue"
    static let ShowAddCaregiver = "ShowAddCaregiverSegue"
}

struct APNConfig {
    static let BaseURL = NSURL(string: "https://fcm.googleapis.com/fcm/send")!
    static let APIKey = "AIzaSyDpJHxVo-LcwG0lB7ApqEiQODtDKwsq5Rk"
    static let SenderID = "428284896059"
}

enum DBPathType: String {
    case Patient = "patients"
    case Caregiver = "caregivers"
}

struct Constants {
    static let EmailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"

    #if Patient
    static let DBPath = DBPathType.Patient.rawValue
    #else
    static let DBPath = DBPathType.Caregiver.rawValue
    #endif
}

struct DBPaths {
    static let GetPatients = ""
    
    static func patientCaregivers(userID: String) -> String {
        return "\(DBPathType.Patient.rawValue)/\(userID)/caregivers"
    }
    
    static func patientNotifications(userID: String) -> String {
        return "\(DBPathType.Patient.rawValue)/\(userID)/notifications"
    }
    
    static func caregiverPatients(userID: String) -> String {
        return "\(DBPathType.Caregiver.rawValue)/\(userID)/patients"
    }
    
    static func caregiver(userID: String) -> String {
        return "\(DBPathType.Caregiver.rawValue)/\(userID)"
    }
}

