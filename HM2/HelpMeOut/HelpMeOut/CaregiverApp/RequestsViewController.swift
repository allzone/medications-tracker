//
//  RequestsViewController.swift
//  HelpMeOut
//
//  Created by Ion Brumari on 9/26/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit
import Firebase

class RequestsViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var content:[ListItemModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Requests"
        loadContent()
    }
    
    func loadContent() {
        guard let currentUser = FIRAuth.auth()?.currentUser else {
            return
        }
        
        FIRManager.shared.loadFromTable(DBPaths.caregiverPatients(currentUser.uid)) { (results) in
            self.content.removeAll()
            for (_,item) in results {
                let patient = ListItemModel(dict: item as! NSDictionary)
                if !patient.approved {
                    self.content.append(patient)
                }
            }
            self.tableView.reloadData()
        }
    }
    
    func updatePatientStatus(index: Int, status: Bool) {
        
        guard let currentUser = FIRAuth.auth()?.currentUser else {
            return
        }
        
        let patientModel = content[index]
        let patientID = patientModel.userID
        
        var data = [:]
        
        if status {
            data = ["aproved":true]
            
            FIRManager.shared.saveRecordTo("\(DBPathType.Patient.rawValue)/\(patientID)/caregivers/\(currentUser.uid)", data: data)
            FIRManager.shared.saveRecordTo("\(DBPathType.Caregiver.rawValue)/\(currentUser.uid)/patients/\(patientID)", data: data)
        } else {
            FIRManager.shared.deleteRecordTo("\(DBPathType.Patient.rawValue)/\(patientID)/caregivers/\(currentUser.uid)")
            FIRManager.shared.deleteRecordTo("\(DBPathType.Caregiver.rawValue)/\(currentUser.uid)/patients/\(patientID)")
        }
    }
    
    // MARK: IBActions
    
    @IBAction func acceptRequest(sender: UIButton) {
        updatePatientStatus(sender.tag, status: true)
    }
    
    @IBAction func declineRequest(sender: UIButton) {
        updatePatientStatus(sender.tag, status: false)
    }
    
    // MARK: Tableview delegate & datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath) as! RequestCell
        cell.approveBtn.tag = indexPath.row
        cell.declineBtn.tag = indexPath.row
        cell.cellTitle.text = content[indexPath.row].userEmail
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
    }
}

