//
//  PatientsListVC.swift
//  HelpMeOut
//
//  Created by Ion Brumari on 9/26/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class PatientsListVC: BaseListViewController {

    @IBOutlet var caregiverID: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "My patients"
        
        refreshControl.addTarget(self, action: #selector(loadContent), forControlEvents: .ValueChanged)
        loadContent()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.hidden = false
    }
    
    func loadContent() {
        guard let currentUser = FIRAuth.auth()?.currentUser else {
            return
        }

        let userID = currentUser.uid
        
        FIRManager.shared.loadFromTableOnce(DBPaths.caregiver(userID), completion: { (results) in
            if let identifier = results["userIdentifier"] as? NSNumber {
                self.caregiverID.text = "My identifier: \(identifier.integerValue)"
            }
        })

        FIRManager.shared.loadFromTable(DBPaths.caregiverPatients(userID)) { (results) in
            self.dataSource.content.removeAll()
            
            for (_,item) in results {
                let caregiver = ListItemModel(dict: item as! NSDictionary)
                if caregiver.approved {
                    self.dataSource.content.append(caregiver)
                }
            }
            
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
}

