//
//  ListItemModel.swift
//  HelpMeOut
//
//  Created by allzone on 9/25/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit

struct ListItemModel {
    let userEmail: String
    let userID: String
    let approved: Bool
    
    init(dict: NSDictionary) {
        userEmail = String(dict["email"]!)
        userID = String(dict["userId"]!)
        approved = dict["aproved"]!.boolValue ?? false
    }
}