//
//  RequestCell.swift
//  HelpMeOut
//
//  Created by Ion Brumari on 9/26/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit

class RequestCell: UITableViewCell {
    @IBOutlet var cellTitle: UILabel!
    @IBOutlet var approveBtn: UIButton!
    @IBOutlet var declineBtn: UIButton!
}
