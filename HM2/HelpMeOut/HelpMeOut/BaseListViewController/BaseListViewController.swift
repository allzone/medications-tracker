//
//  BaseListViewController.swift
//  HelpMeOut
//
//  Created by Ion Brumari on 9/26/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit
import Firebase

class BaseListViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    let dataSource = UserListDataSource()
    let refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let leftBtn = UIBarButtonItem(title: "SignOut", style: .Plain, target: self, action: #selector(self.singOut))
        navigationItem.leftBarButtonItem = leftBtn

        tableView.dataSource = dataSource
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        tableView.addSubview(refreshControl)
    }
    
    @IBAction func singOut() {
        try! FIRAuth.auth()?.signOut()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
