//
//  LoginViewController.swift
//  HelpMeOut
//
//  Created by Ion Brumari on 9/23/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet var userEmailField: UITextField!
    @IBOutlet var userPasswordField: UITextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        FIRManager.shared.checkUserIsLoggedin { 
            self.performSegueWithIdentifier(Segues.ShowProfile, sender: self)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.hidden = true
    }
    
    func validateFields() -> Bool {
        if userEmailField.text?.isEmpty == true ||
            userPasswordField.text?.isEmpty == true {
            return false
        }
        
        return true
    }
    
    func createPatientProfile(currentUser: FIRUser) {
        
        let key = currentUser.uid
        var user: NSDictionary = [:]
        
        #if Patient
            user = [key : ["userID":currentUser.uid,
                            "userEmail":currentUser.email!]]
        #else
            let randomID = Int(arc4random_uniform(UInt32(99999)))
            let deviceToken = FIRInstanceID.instanceID().token() ?? ""
            user = [key : ["userID":currentUser.uid,
                            "userEmail":currentUser.email!,
                            "userIdentifier":randomID,
                            "deviceToken": deviceToken]]
        #endif
        
        FIRManager.shared.saveRecordTo(Constants.DBPath, data: user)
    }
    
    func alertTextFieldDidChange(sender:UITextField) {
        if let alert = presentedViewController as? UIAlertController {
            let email = alert.textFields?.first
            let password = alert.textFields?.last
            
            let emailTest = NSPredicate(format:"SELF MATCHES %@", Constants.EmailRegEx)
            alert.actions.last?.enabled = password?.text?.characters.count > 6 && emailTest.evaluateWithObject(email?.text)
        }
    }
    
    // MARK: IBActions
    
    @IBAction func createAccount() {
        
        let alert = UIAlertController(title: "Create account", message: "", preferredStyle: .Alert)

        alert.addTextFieldWithConfigurationHandler { (email) in
            email.placeholder = "Email"
        }
        
        alert.addTextFieldWithConfigurationHandler { (password) in
            password.placeholder = "Password"
            password.secureTextEntry = true
            password.addTarget(self, action: #selector(self.alertTextFieldDidChange), forControlEvents: .EditingChanged)
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .Cancel, handler: nil)
        let create = UIAlertAction(title: "Create", style: .Default) { (done) in
            let email = alert.textFields?.first?.text
            let password = alert.textFields?.last?.text
            FIRManager.shared.createUser(email!, password: password!, completion: { (user, error) in
                if let _ = user {
                    self.createPatientProfile(user!)
                    self.performSegueWithIdentifier(Segues.ShowProfile, sender: self)
                } else {
                    self.showErrorWarning(error!)
                }
            })
        }
        create.enabled = false
        
        alert.addAction(cancel)
        alert.addAction(create)
        
        presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func login() {
        if validateFields() {
            FIRManager.shared.loginUser(userEmailField.text!, password: userPasswordField.text!, completion: { (user, error) in
                if let _ = user {
                    self.performSegueWithIdentifier(Segues.ShowProfile, sender: self)
                } else {
                    self.showErrorWarning(error!)
                }
            })
        }
    }
}
