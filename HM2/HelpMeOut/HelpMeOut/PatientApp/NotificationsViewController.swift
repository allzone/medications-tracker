//
//  NotificationsViewController.swift
//  HelpMeOut
//
//  Created by Ion Brumari on 9/26/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit
import Firebase

class NotificationsViewController: UIViewController {

    @IBOutlet var tableView: UITableView!
    var content:[Notifications] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Notifications"
        loadContent()
    }
    
    func loadContent() {
        guard let currentUser = FIRAuth.auth()?.currentUser else {
            return
        }

        FIRManager.shared.loadFromTable(DBPaths.patientNotifications(currentUser.uid)) { (results) in
            self.content.removeAll()
            for (_,item) in results {
                let notification = Notifications(dict: item as! NSDictionary)
                self.content.append(notification)
            }
            
            self.tableView.reloadData()
            self.tabBarController?.tabBar.items![1].badgeValue = "\(self.content.count)"
        }
    }
    
    // MARK: Tableview delegate & datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        cell.textLabel?.text = content[indexPath.row].date
        
        return cell
    }
}

