//
//  UserListDataSource.swift
//  HelpMeOut
//
//  Created by allzone on 9/25/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit


class UserListDataSource: NSObject, UITableViewDataSource {
    
    var content:[ListItemModel] = []
    
    // MARK: Tableview datasource
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return content.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Cell", forIndexPath: indexPath)
        cell.textLabel?.text = content[indexPath.row].userEmail
        if content[indexPath.row].approved {
            cell.detailTextLabel?.text = content[indexPath.row].identifier.stringValue
        } else {
            cell.detailTextLabel?.text = "Pending approval"
        }
        
        return cell
    }
}