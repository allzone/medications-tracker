//
//  ListItemModel.swift
//  HelpMeOut
//
//  Created by allzone on 9/25/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit

struct ListItemModel {
    let userEmail: String
    let userID: String
    let identifier: NSNumber
    let approved: Bool
    let deviceToken: String
    
    init(dict: NSDictionary) {
        userEmail = String(dict["email"]!)
        userID = String(dict["userId"]!)
        identifier = dict["identifier"] as! NSNumber
        approved = dict["aproved"]!.boolValue ?? false
        deviceToken = String(dict["deviceToken"]!)
    }
}
