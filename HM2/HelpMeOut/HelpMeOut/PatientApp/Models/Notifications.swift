//
//  Notifications.swift
//  HelpMeOut
//
//  Created by allzone on 9/25/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit

struct Notifications {
//    let userEmail: String
    let date: String
    
    init(dict: NSDictionary) {
//        userEmail = String(dict["toWhom"]!)
        let time = dict["when"]?.integerValue ?? 0
        let d = NSDate(timeIntervalSince1970: NSTimeInterval(time))
        
        let formatter = NSDateFormatter()
        formatter.dateStyle = .LongStyle
        formatter.timeStyle = .MediumStyle
        
        date = formatter.stringFromDate(d)
    }
}

