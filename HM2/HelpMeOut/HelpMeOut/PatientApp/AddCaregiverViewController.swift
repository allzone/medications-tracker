//
//  AddCaregiverViewController.swift
//  HelpMeOut
//
//  Created by allzone on 9/25/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit
import Firebase

class AddCaregiverViewController: UIViewController {
    
    @IBOutlet var caregiverID: UITextField!
    
    override func viewDidLoad() {
        tabBarController?.tabBar.hidden = true
    }

    @IBAction func addCaregiver() {
        
        if caregiverID.text?.isEmpty == true {
            return
            
        }
        
        guard let currentUser = FIRAuth.auth()?.currentUser else {
            return
        }
        
        FIRManager.shared.loadFromTableOnce(DBPathType.Caregiver.rawValue) { (results) in
            var caregiver: NSDictionary = [:]
            var caregiverID = ""
            for (key, item) in results {
                if let identifier = item["userIdentifier"] as? NSNumber where identifier.stringValue == self.caregiverID.text! {
                    let deviceToken = item["deviceToken"] as! String
                    let email = item["userEmail"] as! String
                    caregiverID = key as! String
                    caregiver = ["\(key)":["aproved": false,
                                            "email": email,
                                            "identifier": identifier,
                                            "userId": key,
                                            "deviceToken": deviceToken]]
                }
            }
            
            if caregiver.count > 0 {
                let patient = ["\(currentUser.uid)":["aproved": false,
                                                    "email": currentUser.email!,
                                                    "userId": currentUser.uid]]
                FIRManager.shared.saveRecordTo(DBPaths.patientCaregivers(currentUser.uid), data: caregiver)
                FIRManager.shared.saveRecordTo(DBPaths.caregiverPatients(caregiverID), data: patient)
            } else {
                // show error
                self.showAlert("Error", message: "No records found")
            }
            
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
}
