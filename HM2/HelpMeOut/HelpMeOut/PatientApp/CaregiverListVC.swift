//
//  CaregiverListVC.swift
//  HelpMeOut
//
//  Created by Ion Brumari on 9/26/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class CaregiverListVC: BaseListViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let rightBtn = UIBarButtonItem(barButtonSystemItem: .Add, target: self, action: #selector(addCaregiver))
        
        navigationItem.rightBarButtonItem = rightBtn
        
        title = "My Caregivers"

        refreshControl.addTarget(self, action: #selector(loadContent), forControlEvents: .ValueChanged)
        loadContent()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tabBarController?.tabBar.hidden = false
    }
    
    
    func loadContent() {
        guard let currentUser = FIRAuth.auth()?.currentUser else {
            return
        }
        
        let userID = currentUser.uid
        
        FIRManager.shared.loadFromTable(DBPaths.patientCaregivers(userID)) { (results) in
            self.dataSource.content.removeAll()
            
            for (_,item) in results {
                let caregiver = ListItemModel(dict: item as! NSDictionary)
                self.dataSource.content.append(caregiver)
            }
            
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }        
    }
    
    
    @IBAction func addCaregiver() {
        navigationController?.tabBarController?.hidesBottomBarWhenPushed = true
        performSegueWithIdentifier(Segues.ShowAddCaregiver, sender: self)
    }
    
    @IBAction func callForHelp() {
        guard let currentUser = FIRAuth.auth()?.currentUser else {
            return
        }

        let time = NSDate().timeIntervalSince1970
        let key = "\(time)".stringByReplacingOccurrencesOfString(".", withString: "")
        let user: NSDictionary = [key: ["when": time]]

        FIRManager.shared.loadFromTable(DBPaths.patientCaregivers(currentUser.uid)) { (results) in
            for (_, item) in results {
                let caregiver = ListItemModel(dict: item as! NSDictionary)
                if caregiver.approved {
                    let nBody = currentUser.email! + " needs your help"
                    FIRManager.shared.notifyCaregivers(currentUser.uid, notification: (title: "HELP", body: nBody), deviceToken: caregiver.deviceToken, handler: { (response, error) in
                        if let e = error {
                            self.showErrorWarning(e)
                        }
                    })

                    FIRManager.shared.saveRecordTo(DBPaths.patientNotifications(currentUser.uid), data: user)
                }
            }
        }
    }
}

