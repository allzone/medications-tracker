//
//  UIViewController+Extension.swift
//  HelpMeOut
//
//  Created by allzone on 9/25/16.
//  Copyright © 2016 Ion Brumari. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showErrorWarning(error: NSError) {
        showAlert("Error", message: error.localizedDescription)
    }
    
    
    func showAlert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .Alert)
        let cancelAction = UIAlertAction(title: "Ok", style: .Cancel, handler: nil)
        alert.addAction(cancelAction)
        
        presentViewController(alert, animated: true, completion: nil)
    }
}
